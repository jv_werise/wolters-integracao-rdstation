/*******************************************************************************
*                               Werise - 2020
*-------------------------------------------------------------------------------
*
*	Trigger on Lead SObject for (Insert or Update) && (After or Before)
*
*   Test Class : RSstationAPI_test
*
* NAME: LeadTrigger
* AUTHOR: João Vitor Ramos                                     DATE: 11/11/2020
*******************************************************************************/

trigger LeadTrigger on Lead (before insert, before update, after insert, after update) {
    
    System.debug('################ LEAD TRIGGER IS RUNNING ################');
    /*Set<Id> Cases = new Set<Id>();*/
    
    //If trigger is insert
    if(Trigger.isInsert){
        
        //before insert
        if(Trigger.isBefore){
            System.debug('################ LEAD TRIGGER IS RUNNING BEFORE INSERT ################');
        }
        
        //after insert
        if(Trigger.isAfter){
            System.debug('################ LEAD TRIGGER IS RUNNING AFTER INSERT ################');
        }
    }

    //If trigger is update
    else if(Trigger.isUpdate){
        
        //before update
        if(Trigger.isBefore){
            System.debug('################ LEAD TRIGGER IS RUNNING BEFORE UPDATE ################');
            
            for(Lead newLead : Trigger.new){
                Lead oldLead = Trigger.oldMap.get(newLead.Id);
                
                //Updating Classificacao_do_Lead__c with the new LeadSource value
                if(oldLead.LeadSource != newLead.LeadSource || newLead.Conversoes_RDstation__c == null){
                    if(newLead.Conversoes_RDstation__c == null && newLead.LeadSource != null){
                        newLead.Conversoes_RDstation__c = newLead.LeadSource;
                    }
                    else if((!newLead.Conversoes_RDstation__c.contains(newLead.LeadSource)) && newLead.LeadSource != null){
                        newLead.Conversoes_RDstation__c += (', ' + newLead.LeadSource);
                    }
                }

                //backing the status to the old value if the new value is Novo or Em desenvolvimento
                if((oldLead.Status == 'Aberto' || oldLead.Status == 'Em Desenvolvimento') &&
                (newLead.Status == 'Novo' || newLead.Status == 'Nutrido')){
                    newLead.Status = oldLead.Status;

                    if(newLead.leadSource != oldLead.LeadSource || (oldLead.LeadSource ==  null && 
                    newLead.leadSource != null)){
                        newLead.leadSource = oldLead.LeadSource;
                    }
                }

                //Verify if the Status was changed to 'Nova Nutrição' or 'Sem Interesse' and send the conversion to RD
                if((oldLead.Status != 'Nova Nutrição' && newLead.Status == 'Nova Nutrição') ||
                (oldLead.Status != 'Sem Interesse' && newLead.Status == 'Sem Interesse')){
                    RDStation_SendLeadsRESTAPI.SendBadLeadToRD(newLead.Email, newLead.Status, newLead.Comentario__c);
                }
                
                //Verify if the Status was changed to 'Qualificado' and send the conversion to RD
                if(oldLead.Status != 'Qualificado' && newLead.Status == 'Qualificado'){
                    RDStation_SendLeadsRESTAPI.SendGoodLeadToRD(newLead.Email, newLead.Status);
                }
            }
            
        }
        
        //after update
        if(Trigger.isAfter){
            System.debug('################ LEAD TRIGGER IS RUNNING AFTER UPDATE ################');
            
            
        }
    }
}