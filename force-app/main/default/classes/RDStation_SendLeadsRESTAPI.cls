/*******************************************************************************
*                               Werise - 2020
*-------------------------------------------------------------------------------
*
*	Class that receive lead's informations and send its status to RD station
*
*   Test Class : RSstationAPI_test
*   Called by: LeadTrigger   
*
* NAME: RDStation_SendLeadsRESTAPI
* AUTHOR: João Vitor Ramos                                     DATE: 11/11/2020
*******************************************************************************/

public class RDStation_SendLeadsRESTAPI { 
    
    @future (callout=true)
    public static void SendBadLeadToRD(String email, String status, String comentario) {
        
        //Retrieving the API connection keys to send the information to RD station
        RDStation_Auth__c validToken = getvalidToken();
        
        //setting the endpoint and making the API request
        String url = 'https://api.rd.services/platform/contacts/email:' + email;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('PATCH');
        req.setEndpoint(url);
        req.setBody('{"bio":"'+comentario+'","cf_status":"'+status+'"}');
        req.setHeader('Authorization', 'Bearer ' + validToken.Access_token__c);
        req.setHeader('Accept', 'application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('charset', 'utf-8');
        HttpResponse res = h.send(req);
        
        System.debug(res);
        System.debug(res.getHeaderKeys());
        System.debug(res.getBody());

    }

    @future (callout=true)
    public static void SendGoodLeadToRD(String email, String status){
        //Retrieving the API connection keys to send the information to RD station
        RDStation_Auth__c validToken = getvalidToken();

        //setting the endpoint and making the API request
        String url = 'https://api.rd.services/platform/contacts/email:' + email;
        
        Http h = new Http();
        HttpRequest req1 = new HttpRequest();
        req1.setMethod('PATCH');
        req1.setEndpoint(url);
        req1.setBody('{"cf_status":"'+status+'"}');
        req1.setHeader('Authorization', 'Bearer ' + validToken.Access_token__c);
        req1.setHeader('Accept', 'application/json');
        req1.setHeader('Content-Type', 'application/json');
        req1.setHeader('charset', 'utf-8');
        HttpResponse res = h.send(req1);
        
        System.debug(res);
        System.debug(res.getBody());

        if(res.getStatusCode() == 200){
            //Marking the lead as an Opportunity
            url = 'https://api.rd.services/platform/events';

            HttpRequest req2 = new HttpRequest();
            req2.setMethod('POST');
            req2.setEndpoint(url);
            req2.setBody('{"event_type":"OPPORTUNITY","event_family":"CDP","payload": {"email":"'+email+'","funnel_name": "default"}}');
            req2.setHeader('Authorization', 'Bearer ' + validToken.Access_token__c);
            req2.setHeader('Accept', 'application/json');
            req2.setHeader('Content-Type', 'application/json');
            req2.setHeader('charset', 'utf-8');
            res = h.send(req2);

            System.debug(res);
            System.debug(res.getBody());
        }
    }

    public static RDStation_Auth__c getvalidToken(){
        //retrieving the record with the value of API connection keys
        RDStation_Auth__c validToken = new RDStation_Auth__c();
        if(Test.isRunningTest()){
            validToken.Id = 'a102C000001hiZZQAY';
            validToken.Name = 'Valid_token';
            validToken.Access_token__c = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwczovL2FwaS5yZC5zZXJ2aWNlcyIsInN1YiI6IlpQRFhxNWM1SnRwREx4OWd0c0ljeGNRU24zM1ZJQ2txVi0wdld3R2FFSEFAY2xpZW50cyIsImF1ZCI6Imh0dHBzOi8vYXBwLnJkc3RhdGlvbi5jb20uYnIvYXBpL3YyLyIsImFwcF9uYW1lIjoiU2FsZXNmb3JjZSBSRCBBUEkgSW50ZWdyYXRpb24iLCJleHAiOjE2MDUyMDE2MzAsImlhdCI6MTYwNTExNTIzMCwic2NvcGUiOiIifQ.bIuLzBxJc0s21yp6c3jlo5ES5sYsRGS_DQpCR8mCgnLqQytnl8TwM0PZyWPsrkLnOiYCvy37kM_MPOlVy-B_MXVZ32ag42v9pq6CNYhYTlFO7rleX13At_VJ7v9-NJxXwybOAHjQoDBQmbaBuUFdR5EjukrfcTp-PdW9puEwl2rhosZ_4dZEsS0ZheNDb2gTZIuEVWxA6Kf5mKi3pY9CmxdKFGVoTLUGPKuL6MFAPKd3Jm-5Uq6g6uN1cTTpzZSYuzxr-6ikMQahiSbT7BxshVAnobos6i6OBXFD8Eiytx33BdhlksW0asGSZR6Xcb6X4P4r6yDVeQDO3l0mnYRkjw';
            validToken.Client_id__c = '9a9424ec-82db-4faa-a10c-5d1c19c29d50';
            validToken.Client_secret__c = '184b6e2aaaed4c59a721ec5f27263568';
            validToken.Code__c = 'ea9fb42b9559a728ad6232368f802f1d';
            validToken.Refresh_token__c = 'lTqBCWETmObDFkVWsuQbXv4fXqmTlUcrYmYQaUWnASE';
        }
        else{
            validToken = [SELECT Name, Access_token__c, Client_id__c, Client_secret__c, Code__c, Refresh_token__c
            FROM RDStation_Auth__c
            WHERE Name = 'Valid_token'
            LIMIT 1];
        }
        return validToken;
    }
}