/*******************************************************************************
*                               Werise - 2020
*-------------------------------------------------------------------------------
*
*	Class that schedule the RDStation_AuthRESTAPI to get the access token every day
*
*   Test Class : RSstationAPI_test
*   Call:RDStation_AuthRESTAPI
*
* NAME: RDstationAuthSchedule
* AUTHOR: João Vitor Ramos                                     DATE: 11/11/2020
*******************************************************************************/
global class RDstationAuthSchedule implements Schedulable {
    global void execute(SchedulableContext ctx) {
        RDStation_AuthRESTAPI.GetAccessToken();
    }
}
