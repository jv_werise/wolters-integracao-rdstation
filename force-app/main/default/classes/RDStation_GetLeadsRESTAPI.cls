/*******************************************************************************
*                               Werise - 2020
*-------------------------------------------------------------------------------
*   Temporariamente não é mais usada, lógica da API trocada.(10/11/2020)
*	Class RDStation_GetLeadsRESTAPI
*
*   Test Class : 
*
* NAME: RDStation_GetLeadsRESTAPI
* AUTHOR: Raphael Silva                                        DATE: 14/07/2020
*******************************************************************************/

global class RDStation_GetLeadsRESTAPI { 
    
    //@future (callout=true)
    public static List<String> GetLeadByEmail(String leadEmail) {
        
        RDStation_Auth__c validToken = new RDStation_Auth__c();
        validToken = [SELECT Name, Access_token__c, Client_id__c, Client_secret__c, Code__c, Refresh_token__c
                      FROM RDStation_Auth__c
                      WHERE Name = 'Valid_token'
                      LIMIT 1];
        
        String email = leadEmail;
        String url = 'https://api.rd.services/platform/contacts/email:';
        url += email;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint(url);
        req.setHeader('Authorization', 'Bearer ' + validToken.Access_token__c);
        req.setHeader('Accept', 'application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('charset', 'utf-8');
        HttpResponse res = h.send(req);
        
        System.debug(req);
        System.debug(res);
        
        StringResponse results = (StringResponse)System.JSON.deserialize(res.getBody(), StringResponse.class);
        System.debug(res.getBody());
        System.debug(res.getBody().substringAfter('-'));
        //String tags = 'test'; //String.valueOf(results.get('tags'));
        for(String str : results.tags){
            System.debug(str);
        }
        System.debug(results.tags);
        //System.debug(tags);
        
        return results.tags;
    }
    
    public class StringResponse {
        public String email {get; set;}
        public List<String> tags {get; set;}
    }
}