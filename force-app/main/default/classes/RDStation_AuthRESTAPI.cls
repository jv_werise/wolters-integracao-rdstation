/*******************************************************************************
*                               Werise - 2020
*-------------------------------------------------------------------------------
*
*	Class that retrieve the Access token keys to connect the salesforce with RD station
*
*   Called by: RDstationAuthSchedule
*   Test Class :RSstationAPI_test 
*
* NAME: RDStation_AuthRESTAPI
* AUTHOR: João Vitor Ramos                                       DATE: 11/11/2020
*******************************************************************************/

public class RDStation_AuthRESTAPI {

    @future(Callout=true)
    public static void GetAccessToken() {

        //retrieving the record with the value of API connection keys
        RDStation_Auth__c validToken = new RDStation_Auth__c();
        if(Test.isRunningTest()){
            validToken.Id = 'a102C000001hiZZQAY';
            validToken.Name = 'Valid_token';
            validToken.Access_token__c = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwczovL2FwaS5yZC5zZXJ2aWNlcyIsInN1YiI6IlpQRFhxNWM1SnRwREx4OWd0c0ljeGNRU24zM1ZJQ2txVi0wdld3R2FFSEFAY2xpZW50cyIsImF1ZCI6Imh0dHBzOi8vYXBwLnJkc3RhdGlvbi5jb20uYnIvYXBpL3YyLyIsImFwcF9uYW1lIjoiU2FsZXNmb3JjZSBSRCBBUEkgSW50ZWdyYXRpb24iLCJleHAiOjE2MDUyMDE2MzAsImlhdCI6MTYwNTExNTIzMCwic2NvcGUiOiIifQ.bIuLzBxJc0s21yp6c3jlo5ES5sYsRGS_DQpCR8mCgnLqQytnl8TwM0PZyWPsrkLnOiYCvy37kM_MPOlVy-B_MXVZ32ag42v9pq6CNYhYTlFO7rleX13At_VJ7v9-NJxXwybOAHjQoDBQmbaBuUFdR5EjukrfcTp-PdW9puEwl2rhosZ_4dZEsS0ZheNDb2gTZIuEVWxA6Kf5mKi3pY9CmxdKFGVoTLUGPKuL6MFAPKd3Jm-5Uq6g6uN1cTTpzZSYuzxr-6ikMQahiSbT7BxshVAnobos6i6OBXFD8Eiytx33BdhlksW0asGSZR6Xcb6X4P4r6yDVeQDO3l0mnYRkjw';
            validToken.Client_id__c = '9a9424ec-82db-4faa-a10c-5d1c19c29d50';
            validToken.Client_secret__c = '184b6e2aaaed4c59a721ec5f27263568';
            validToken.Code__c = 'ea9fb42b9559a728ad6232368f802f1d';
            validToken.Refresh_token__c = 'lTqBCWETmObDFkVWsuQbXv4fXqmTlUcrYmYQaUWnASE';
        }
        else{
            validToken = [SELECT Name, Access_token__c, Client_id__c, Client_secret__c, Code__c, Refresh_token__c
            FROM RDStation_Auth__c
            WHERE Name = 'Valid_token'
            LIMIT 1];
        }
    	
        //Requesting the updated keys to RD station                    
        String reqbody = 'client_id='+validToken.Client_id__c+'&client_secret='+validToken.Client_secret__c+'&refresh_token='+validToken.Refresh_token__c;
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setBody(reqbody);
        req.setMethod('POST');
        req.setEndpoint('https://api.rd.services/auth/token');
        HttpResponse res = h.send(req);
        
        //Updating the record with the new Access tokens
        if(res.getStatusCode() == 200){
            Map<String, Object> results = (Map<String, Object>)System.JSON.deserializeUntyped(res.getBody());
            validToken.Access_token__c = String.valueOf(results.get('access_token'));
            validToken.Refresh_token__c = String.valueOf(results.get('refresh_token'));
            System.debug(String.valueOf(results.get('access_token')));
            
            update validToken;
        }
    }
}