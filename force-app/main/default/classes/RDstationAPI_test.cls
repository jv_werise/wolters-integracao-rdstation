/*******************************************************************************
*                               Werise - 2020
*-------------------------------------------------------------------------------
*
*	Test class to test the lead trigger on update and API auth, 
*   targeting the RDstation interactions
*
*   Test who: LeadTrigger, RDStation_SendLeadsRESTAPI, RDStation_AuthRESTAPI 
*   and RDstationAuthSchedule
*
* NAME: RSstationAPI_test
* AUTHOR: João Vitor Ramos                                     DATE: 11/11/2020
*******************************************************************************/
@isTest
public class RDstationAPI_test {
    
    private static List<Lead> makeData(){
        List<Lead> leadList = new List<Lead>();

        //Making the Accounnt to match with the leads
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.Official_Name__c = acc.Name;
        acc.Internal_Code__c = '101010';
        acc.Status__c = 'Active';
        acc.form_of_payment__c = 'Boleto Bancario';
        acc.RecordTypeId = [SELECT Id FROM RecordType WHERE name = 'Partner'].Id;
        insert acc;

        //making the leads to test with all information to be able to qualify
        for(Integer i=0; i<10; i++){
            Lead ld = new Lead();
            ld.FirstName = 'Test'+i;
            ld.LastName = 'Lead';
            ld.Company = acc.Name;
            ld.Email = 'test'+i+'@test.com';
            ld.Status = 'Novo';
            ld.Status__c = 'Ativo';
            ld.LeadSource = 'Advertisement';
            ld.Rating = 'Cold';
            ld.CPF_CNPJ__c = '95268585061'; //made by 4devs CPF generator, it isn't a real CPF
            ld.Tipo_Informacao_do_Contato__c = 'Owner';
            ld.Tipo__c = 'Física';
            ld.Canal_Responsavel__c = acc.Id;
            ld.Codigo_do_Canal__c = acc.Internal_Code__c;
            ld.Phone = '11999999999';
            ld.MobilePhone = '11999999999';
            ld.form_of_payment__c = acc.form_of_payment__c;
            ld.CEP__c = '10101101';
            ld.Tipo_de_endereco__c = 'test';
            ld.Endereco__c = 'test';
            ld.Numero__c = '1010';
            ld.Complemento__c = 'test';
            ld.Cidade__c = 'test';
            ld.Bairro__c = 'test';
            ld.Estado__c = 'TE';
            ld.Conversoes_RDstation__c = null;

            leadList.add(ld);
        }
        insert leadList;
        return leadList;
    }

    @isTest
    private static void testConversoes(){
        List<Lead> leadList = makeData();
        List<Lead> leadListUpd = new List<Lead>();
        List<Lead> leadListUpd1 = new List<Lead>();
        List<Lead> leadListUpd2 = new List<Lead>();
        Integer counter = 0;

        update leadList;

        for(Lead ld : leadList){
            if(counter < 5)
                ld.LeadSource = 'Event';
            leadListUpd.add(ld);
            counter++;
        }

        //Updating to fill the logic when the LeadSource field changed
        update leadListUpd;

        leadListUpd1 = [SELECT Conversoes_RDstation__c FROM Lead];

        //Validating
        System.assertEquals(10, leadListUpd1.size(),'list1 errada');
    }

    @isTest
    private static void testStatus(){
        List<Lead> leadList = makeData();
        List<Lead> leadListUpd1 = new List<Lead>();
        List<Lead> leadListUpd2 = new List<Lead>();

        for(Integer i=0; i<leadList.size(); i++){
            if(i < 5)
                leadList[i].Status = 'Em Desenvolvimento';
            else
                leadList[i].Status = 'Aberto';
        }
        update leadList;

        for(Integer i=0; i<leadList.size(); i++){
            if(i < 5)
                leadList[i].Status = 'Novo';
            else
                leadList[i].Status = 'Nutrido';
            leadList[i].leadSource = 'Other';
        }
        update leadList;

        leadListUpd1 = [SELECT id FROM Lead where Status = 'Em Desenvolvimento' OR Status = 'Aberto'];
        leadListUpd2 = [SELECT id FROM Lead where leadSource = 'Advertisement'];

        System.assertEquals(10, leadListUpd1.size());
        System.assertEquals(10, leadListUpd2.size());
    }

    @isTest
    private static void testBadleads(){
        List<Lead> leadList = makeData();
        List<Lead> leadListUpd = new List<Lead>();
        
        //Updating to fire the lead trigger
        for(Integer i=0; i<leadList.size(); i++){
            if(i < 5){
                leadList[i].Status = 'Nova Nutrição';
                leadList[i].Comentario__c = 'virou Nova Nutrição';
                leadList[i].Motivo__c = 'Estudante';
            }
            else{
                leadList[i].Status = 'Sem Interesse';
                leadList[i].Comentario__c = 'Ficou Sem Interesse';
                leadList[i].Motivo__c = 'Estudante';
            }
        }
        update leadList;

        //Calling the MockCallout class to test the API
        Test.setMock(HttpCalloutMock.class, new RDstationCalloutMock());
        RDStation_SendLeadsRESTAPI.SendBadLeadToRD('teste@teste.com', 'Sem Interesse', 'Não tem mais interesse');

        leadListUpd = [SELECT id FROM Lead where Status = 'Nova Nutrição' OR Status = 'Sem Interesse'];

        System.assertEquals(10, leadListUpd.size());

    }

    @isTest
    private static void testGoodLeads(){
        List<Lead> leadList = makeData();
        List<Lead> leadListUpd = new List<Lead>();

        //Updating to fire the lead trigger
        for(Integer i=0; i<leadList.size(); i++){
            leadList[i].Status = 'Qualificado';
        }
        update leadList;

        //Calling the MockCallout class to test the API
        Test.setMock(HttpCalloutMock.class, new RDstationCalloutMock());
        RDStation_SendLeadsRESTAPI.SendGoodLeadToRD('teste@teste.com', 'Qualificado');

        leadListUpd = [SELECT id FROM Lead where Status = 'Qualificado'];

        System.assertEquals(10, leadListUpd.size());       
    }

    @isTest
    private static void testAuth(){
        //creating crono variable to schedule the class
        String CRON_EXP = '0 0 0 15 3 ? 2022';

        // Schedule the test job
        Test.startTest();          
            String jobId = System.schedule('ScheduledAuthTest', CRON_EXP, new RDstationAuthSchedule());

        //Calling the MockCallout class to test the API
        Test.setMock(HttpCalloutMock.class, new RDstationAuthCalloutMock());
        RDStation_AuthRESTAPI.GetAccessToken();
        Test.stopTest();

    }
}
