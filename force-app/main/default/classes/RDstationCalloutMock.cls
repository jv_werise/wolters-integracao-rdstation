/*******************************************************************************
*                               Werise - 2020
*-------------------------------------------------------------------------------
*
*	Test class to Mock the API call of RDStation_SendLeadsRESTAPI Class 
*   targeting the RDstation interactions
*
*   Called by: RSstationAPI_test
*
* NAME: RDstationCalloutMock
* AUTHOR: João Vitor Ramos                                     DATE: 11/11/2020
*******************************************************************************/
@isTest
global class RDstationCalloutMock implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest request) {
        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"email":"teste@teste.com","name":"teste","job_title":"testador","bio":"Não tem mais interesse",' +
        '"uuid":"64a8934b-7efb-447c-aba8-048ee27beae2","state":"RJ","city":"Acrelândia","personal_phone":"11999999898","tags":["teste","teste2"]');
        response.setStatusCode(200);
        return response; 
    }
}