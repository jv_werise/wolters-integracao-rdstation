/*
*This class will be referenced to create row data
*/
@isTest
public class TestDataFactory {

    //Create Partner Account 
    public static Account createPartnerAccount(string Name, string officialName, String internalCode, String recordType, String billingZip, String ShippingZip, Integer index)
    {
        Account  acc =new Account();
        acc.Name = Name + index;
        acc.Official_Name__c = officialName + index;
        acc.Internal_Code__c = internalCode + index;
        acc.recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(recordType).getRecordTypeId();
        acc.Billing_Zipcode__c = billingZip;
        acc.Shipping_Zipcode__c = ShippingZip;
        return acc;
    }
    
    //Create Prospect/Customer Account
    public static Account createAccount(string Name, string officialName, String internalCode, String Type, String CPFCNPJ, String CNAE, String StateRegistration, String Email, String Phone, String Status, Account resPartner, String recordType, Integer Index)
    {
        Account  acc =new Account();
        acc.Name = Name + index;
        acc.Official_Name__c = officialName + index;
        acc.Internal_Code__c = internalCode + index;
        acc.Type = Type;
        acc.CPF_CNPJ__c = CPFCNPJ;
        acc.CNAE__c = CNAE;
        acc.State_Registration__c = StateRegistration;
        acc.Email__c = Email;
        acc.Phone = Phone;
        acc.Status__c = Status;
        acc.Responsible_Channel_Partner__c = resPartner.Id;
        acc.recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(recordType).getRecordTypeId();
        acc.Billing_Zipcode__c  ='92659265';
        acc.Billing_Address_Type__c  ='COMERCIAL';
        acc.Billing_Address_Line__c  ='Box 8344';
        acc.Billing_Address_Number__c  ='750';
        acc.Billing_Address_Comp__c  ='CASA';
        acc.Billing_City__c  ='Newport Beach';
        acc.Billing_District__c  ='DISTRO FEDERALE';
        acc.Billing_State__c  ='CA';
        acc.Shipping_Zipcode__c  ='96589265';
        acc.Shipping_Address_Type__c  ='R';
        acc.Shipping_Address_Line__c  ='Box 8344';
        acc.Shipping_Address_Number__c  ='750';
        acc.Shipping_Address_Complement__c  ='CASA';
        acc.Shipping_City__c ='Newport Beach';
        acc.Shipping_District__c ='DISTRO FEDERALE';
        acc.Shipping_State__c ='CA';
        return acc;
    }
    
    //Create contact data
    public static Contact createContact(string lastname, string type, String email, Account account, String CPF, String recordType, Integer Index)
    {
        Contact contact = new Contact();
        contact.lastname = lastname + Index;
        contact.type__c = type;
        contact.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(recordType).getRecordTypeId();
        contact.email = email;
        contact.accountid = account.Id;
        contact.CPF__c = CPF;
        return contact;
    }
    
    //Create Product data
    public static Product2 createProduct(string Name, string ProductCode, Boolean Isactive, String description , String refNumber, Integer Index)
    {
        Product2 newProduct = new Product2();
        newProduct.Name = Name;
        newProduct.ProductCode = ProductCode;
        newProduct.IsActive = Isactive;
        newProduct.Description = description;
        newProduct.Reference_Number__c = refNumber;
        return newProduct;
    }
    
    //Create Pricebookentry
    public static PricebookEntry createPriceBookEntry(Boolean Isactive, Decimal unitPrice, Id pricebookId, Product2 prod)
    {
        PricebookEntry newPricebookEntry = new PricebookEntry();
        newPricebookEntry.IsActive = Isactive;
        newPricebookEntry.UnitPrice = unitPrice;
        newPricebookEntry.Pricebook2Id = pricebookId;
        newPricebookEntry.Product2Id = prod.Id;
        return newPricebookEntry;
    }
    
    //Create Opportunity
     public static Opportunity createOpportunity(Account acc, String Name, String Stage, Date closeDate, Id pricebookId, Integer index) {
        Opportunity objOppty = new Opportunity();
        objOppty.AccountId = acc.Id;
        objOppty.Name = Name + index;
        objOppty.StageName = Stage;
        objOppty.Pricebook2Id = pricebookId;
        objOppty.CloseDate = Date.newInstance(2022, 12, 9);
        objOppty.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Product').getRecordTypeId();
        return objOppty;

    }
    
    //Create OpportunityLineItem 
    public static OpportunityLineItem createOpportunityLineItem(Opportunity opp, Integer quantity, Decimal totalPrice, PricebookEntry newPriceBookEntry) {
        OpportunityLineItem objOpptyLine = new OpportunityLineItem();
        objOpptyLine.OpportunityId = opp.Id;
        objOpptyLine.Quantity = quantity;
        objOpptyLine.TotalPrice = totalPrice;
        objOpptyLine.PricebookEntryId = newPricebookEntry.Id;
        return objOpptyLine;

    }
    
    //Create Quote
    public static Quote createQuote(String name, Opportunity opp, String Status, String recordType, String Concorrente, String months, Id priceBookId, Integer index) {
       Quote quote = new Quote();
       quote.Name = name + index;
       quote.Recordtypeid = Schema.SObjectType.Quote.getRecordTypeInfosByName().get(recordType).getRecordTypeId();
       quote.OpportunityId = opp.Id;
       quote.First_installment_is_due_on__c = Date.newInstance(2022, 12, 9);
       quote.Status = Status;
       quote.Concorrente__c = Concorrente;
       quote.Pricebook2Id = pricebookId;
       quote.Deferment_of_Payment_in_months__c = months;
       return quote;

    }
    
    //Create QuoteLineItem 
    public static QuoteLineItem createQuoteLineItem(Quote quote, PriceBookEntry newPricebookEntry, Integer quantity, Decimal unitPrice) {
       QuoteLineItem quoteItem = new QuoteLineItem();
       quoteItem.QuoteId = quote.Id;
       quoteItem.PricebookEntryId = newPricebookEntry.Id;
       quoteItem.Quantity = quantity;
       quoteItem.UnitPrice = unitPrice;
       return quoteItem;

    }
    
    //Create Order data
    public static Order createOrder(Account acc, Quote quoteRecord, String Status, Opportunity opp, Id priceBookId, Integer index) {
       Order orderRecord = new Order();
       orderRecord.AccountId = acc.Id;
       orderRecord.Status = Status;
       orderRecord.QuoteId = quoteRecord.Id;
       orderRecord.EffectiveDate = System.Today();
       orderRecord.Pricebook2Id = pricebookId;
       orderRecord.OpportunityId = quoteRecord.OpportunityId;
       orderRecord.OrderKey__c = 'ORD-000027-UUID';
       orderRecord.Billing_Address_Complement__c = acc.Billing_Address_Comp__c;
       orderRecord.Billing_Address_Number__c = acc.Billing_Address_Number__c;
       orderRecord.Billing_Address_Line__c = acc.Billing_Address_Line__c;
       orderRecord.Billing_Address_Type__c = acc.Billing_Address_Type__c;
       orderRecord.Billing_City__c = acc.Billing_City__c;
       orderRecord.Billing_District__c = acc.Billing_District__c;
       orderRecord.Billing_State__c = acc.Billing_State__c;
       orderRecord.Billing_Zipcode__c = acc.Billing_Zipcode__c;
       orderRecord.Shipping_Address_Complement__c = acc.Shipping_Address_Complement__c;
       orderRecord.Shipping_Address_Line__c = acc.Shipping_Address_Line__c;
       orderRecord.Shipping_Address_Number__c = acc.Shipping_Address_Number__c;
       orderRecord.Shipping_City__c = acc.Shipping_City__c;
       orderRecord.Shipping_District__c = acc.Shipping_District__c;
       orderRecord.Shipping_State__c = acc.Shipping_State__c;
       orderRecord.Shipping_Zipcode__c = acc.Shipping_Zipcode__c;
       orderRecord.Shipping_Address_Type__c = acc.Shipping_Address_Type__c;
       orderRecord.Forma_de_Pagamento__c = '1';
       orderRecord.Service_Location__c = 'Remote';
       return orderRecord;

    }
    
    //Create orderItem data
    public static OrderItem createOrderItem(Order order, PriceBookEntry newPricebookEntry, Integer quantity, Decimal unitPrice) {
       OrderItem orderItem = new OrderItem();
       orderItem.OrderId = order.Id;
       orderItem.PricebookEntryId = newPricebookEntry.Id;
       orderItem.Quantity = quantity;
       orderItem.UnitPrice = unitPrice;
       return orderItem;

    }
    
}